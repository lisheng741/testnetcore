﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace FilterOrderTest.Filters
{
    public class TestActionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            Console.WriteLine("TestActionFilter 1");
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            Console.WriteLine("TestActionFilter 2");
        }
    }

    public class TestAsyncActionFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            await Console.Out.WriteLineAsync("action async 1");
            await next();
            await Console.Out.WriteLineAsync("action async 2");
        }
    }
}
