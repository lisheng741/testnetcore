﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace FilterOrderTest.Filters
{
    public class TestResultFilter : IResultFilter
    {
        public void OnResultExecuted(ResultExecutedContext context)
        {
            throw new NotImplementedException();
        }

        public void OnResultExecuting(ResultExecutingContext context)
        {
            throw new NotImplementedException();
        }
    }

    public class TestAsyncResultFilter : IAsyncResultFilter
    {
        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            await Console.Out.WriteLineAsync("R1");
            await next();
            await Console.Out.WriteLineAsync("R2");
        }
    }
}
