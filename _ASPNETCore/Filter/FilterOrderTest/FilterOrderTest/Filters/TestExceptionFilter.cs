﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace FilterOrderTest.Filters
{
    public class TestExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            Console.WriteLine("TestExceptionFilter");
        }
    }

    public class TestAsyncExceptionFilter : IAsyncExceptionFilter
    {
        public Task OnExceptionAsync(ExceptionContext context)
        {
            Console.WriteLine("TestAsyncExceptionFilter");
            return Task.CompletedTask;
        }
    }
}
