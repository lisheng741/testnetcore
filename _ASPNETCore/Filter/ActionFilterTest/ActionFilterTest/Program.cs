using ActionFilterTest.Filters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers(options =>
{
    options.Filters.Add<ResultTestFilter>(-5);
    //options.Filters.Add<Test1ActionFilter>(1); // 这种情况才有效
    options.Filters.Add<Test1ActionFilter>(1);
    //options.Filters.Add<Test2ActionFilter>(-1);
    options.Filters.Add<Test3ActionFilter>(0);
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
