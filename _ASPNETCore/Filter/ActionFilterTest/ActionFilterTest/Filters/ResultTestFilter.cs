﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace ActionFilterTest.Filters
{
    public class ResultTestFilter : IResultFilter, IOrderedFilter
    {
        public int Order { get; set; }

        public void OnResultExecuted(ResultExecutedContext context)
        {
            Console.WriteLine("result Executed");
        }

        public void OnResultExecuting(ResultExecutingContext context)
        {
            Console.WriteLine("result Executing");
        }
    }
}
