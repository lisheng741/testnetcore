﻿using System;
using System.Collections.Generic;

namespace ScaffoldingAndQueryFilterTest.Models
{
    /// <summary>
    /// 操作日志表
    /// </summary>
    public partial class SysLogOperating
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string Id { get; set; } = null!;
        /// <summary>
        /// 操作人
        /// </summary>
        public string? Account { get; set; }
        /// <summary>
        /// 日志名称
        /// </summary>
        public string Name { get; set; } = null!;
        /// <summary>
        /// 是否执行成功
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// 具体消息
        /// </summary>
        public string? Message { get; set; }
        /// <summary>
        /// IP
        /// </summary>
        public string? Ip { get; set; }
        /// <summary>
        /// 浏览器
        /// </summary>
        public string? Browser { get; set; }
        /// <summary>
        /// 操作系统
        /// </summary>
        public string? OperatingSystem { get; set; }
        /// <summary>
        /// 完整请求地址
        /// </summary>
        public string? Url { get; set; }
        /// <summary>
        /// 类名称
        /// </summary>
        public string? ClassName { get; set; }
        /// <summary>
        /// 方法名称
        /// </summary>
        public string? MethodName { get; set; }
        /// <summary>
        /// 请求方式
        /// </summary>
        public string? RequestMethod { get; set; }
        /// <summary>
        /// 返回结果
        /// </summary>
        public string? Result { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTimeOffset OperatingTime { get; set; }
        /// <summary>
        /// 耗时（毫秒）
        /// </summary>
        public long? ElapsedTime { get; set; }
        /// <summary>
        /// 请求Body
        /// </summary>
        public string? Body { get; set; }
        /// <summary>
        /// 请求路径
        /// </summary>
        public string? Path { get; set; }
    }
}
