﻿using System;
using System.Collections.Generic;

namespace ScaffoldingAndQueryFilterTest.Models
{
    public partial class SysJob
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string Id { get; set; } = null!;
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; } = null!;
        /// <summary>
        /// 任务类名（完整名称）
        /// </summary>
        public string ActionClass { get; set; } = null!;
        /// <summary>
        /// Cron表达式
        /// </summary>
        public string Cron { get; set; } = null!;
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? Remark { get; set; }
        /// <summary>
        /// 启用状态
        /// </summary>
        public bool IsEnabled { get; set; }
    }
}
