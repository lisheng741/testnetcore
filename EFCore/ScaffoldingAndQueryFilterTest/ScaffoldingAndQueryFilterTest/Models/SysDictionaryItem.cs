﻿using System;
using System.Collections.Generic;

namespace ScaffoldingAndQueryFilterTest.Models
{
    /// <summary>
    /// 字典子项表
    /// </summary>
    public partial class SysDictionaryItem
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string Id { get; set; } = null!;
        /// <summary>
        /// 字典Id
        /// </summary>
        public string DictionaryId { get; set; } = null!;
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; } = null!;
        /// <summary>
        /// 显示名称
        /// </summary>
        public string Name { get; set; } = null!;
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string? Remark { get; set; }
        /// <summary>
        /// 启用状态
        /// </summary>
        public bool IsEnabled { get; set; }

        public virtual SysDictionary Dictionary { get; set; } = null!;
    }
}
