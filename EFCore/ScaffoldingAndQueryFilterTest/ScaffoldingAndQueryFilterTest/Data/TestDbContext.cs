﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ScaffoldingAndQueryFilterTest.Models;

namespace ScaffoldingAndQueryFilterTest.Data;

public class TestDbContext : SimpleAppTestContext
{
    public TestDbContext(DbContextOptions<SimpleAppTestContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);


        modelBuilder.Entity<SysMenu>().HasQueryFilter(e => EF.Property<int>(e, "Type") == 0);
    }
}