using Microsoft.EntityFrameworkCore;
using ScaffoldingAndQueryFilterTest.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

string constr = builder.Configuration["ConnectionStrings:SqlServer"]!;
builder.Services.AddDbContext<SimpleAppTestContext>(options =>
{
    options.UseSqlServer(constr);
});
//builder.Services.AddDbContext<TestDbContext>(options =>
//{
//    options.UseSqlServer(constr);
//});
builder.Services.AddScoped<TestDbContext>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
