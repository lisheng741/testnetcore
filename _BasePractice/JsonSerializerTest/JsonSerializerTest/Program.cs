﻿// See https://aka.ms/new-console-template for more information
using System.Text.Json;

Console.WriteLine("Hello, World!");

Forecast forecast = new()
{
    Date = DateTime.Now,
    TemperatureC = 40,
    Summary = "Hot"
};



JsonSerializerOptions options = new()
{
    WriteIndented = true
};

JsonSerializerOptions optionsCopy = new(JsonSerializerDefaults.Web);
string forecastJson = JsonSerializer.Serialize<Forecast>(forecast, optionsCopy);

var defaultOptions = JsonSerializerOptions.Default;


Console.WriteLine($"Output JSON:\n{forecastJson}");

public class Forecast
{
    public DateTime Date { get; init; }
    public int TemperatureC { get; set; }
    public string? Summary { get; set; }
};