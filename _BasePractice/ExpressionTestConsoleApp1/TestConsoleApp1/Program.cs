﻿// See https://aka.ms/new-console-template for more information


using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using TestConsoleApp1;

object i = 1;


var f1 = new FormFieldInfo("Day", FormFieldType.Int32, 3);
var f2 = new FormFieldInfo("Day", FormFieldType.Int32, 4);

Console.WriteLine(ExpressionHelper.Condition(f1, f2, ConditionType.Equal));
Console.WriteLine(ExpressionHelper.Condition(f1, f2, ConditionType.GreaterThan));
Console.WriteLine(ExpressionHelper.Condition(f1, f2, ConditionType.LessThan));


var fs1 = new FormFieldInfo("Day", FormFieldType.String, "abc");
var fs2 = new FormFieldInfo("Day", FormFieldType.String, "abc");

Console.WriteLine(ExpressionHelper.Condition(fs1, fs2, ConditionType.Equal));
Console.WriteLine(ExpressionHelper.Condition(fs1, fs2, ConditionType.NotEqual));


Console.WriteLine("Hello, World!");