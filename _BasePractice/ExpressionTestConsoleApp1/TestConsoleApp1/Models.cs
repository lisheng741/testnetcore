﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsoleApp1;

public class FormField2
{
    public string Name { get; set; }

    public Type Type { get; set; }

    public object Value { get; set; }
}

public class Condition
{
    public string FieldName { get; set; }

    public string Type { get; set; } = "System.Int32";

    public object Value { get; set; }

    public string OperatorType { get; set; } = "==";
}
